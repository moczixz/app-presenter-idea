import { ViewAnchorDirective } from './directives/view-anchor.directive';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        ViewAnchorDirective
    ],
    exports: [
        ViewAnchorDirective
    ]
})
export class SharedModule {
    
}