import { ComponentFactoryResolver } from '@angular/core';
import { ViewAnchorDirective } from '../directives/view-anchor.directive';

// this vars set in environments or config
const viewHierarchy = {
  brand2: [
    { brand: 'brand2', app: 'web' },
    { brand: 'brand1', app: 'web' },
    { brand: 'brand1', app: 'mobile' }
  ]
};

const config = {
  brand: 'brand2',
  app: 'web'
};



export class ViewResolver {
  constructor(private componentFactoryResolver: ComponentFactoryResolver, private viewAnchor: ViewAnchorDirective) {
  }

  private resolveFromHierarchy(components: { [key: string]: any }) {
    const hierarchy = viewHierarchy[config.brand];
    const foundTemplate = hierarchy.find(data => {
      return typeof components[`${data.brand}-${data.app}`] !== 'undefined';
    });
    return components[`${foundTemplate.brand}-${foundTemplate.app}`];
  }

  private resolveComponent(components: { [key: string]: any }): any {
    if (components[`${config.brand}-${config.app}`]) {
      return components[`${config.brand}-${config.app}`];
    } else {
      return this.resolveFromHierarchy(components);
    }
  }

  resolveView(components: { [key: string]: any }): void {
    const component = this.resolveComponent(components);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const viewContainerRef = this.viewAnchor.viewContainerRef;
    viewContainerRef.clear();

    viewContainerRef.createComponent(componentFactory);
  }
}
