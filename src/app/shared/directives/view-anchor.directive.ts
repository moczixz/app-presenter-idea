import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[view-anchor]'
})
export class ViewAnchorDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

