import { Component, OnInit, Inject } from '@angular/core';
import { HomePresenterBrand1MobileContract } from '../../home-contract.interface';
import { HomePresenter } from '../../home.presenter';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-home-brand1-mobile]',
  templateUrl: 'home-brand1-mobile.component.html'
})
export class HomeBrand1MobileComponent implements OnInit {

  // @Inject because we want diffrent type for our presenter
  constructor(@Inject(HomePresenter) private homePresenter: HomePresenterBrand1MobileContract) {
    this.homePresenter.setContext(<any>this);
  }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.homePresenter.loadDataFromServer()
      .subscribe(data => {
        // do something here
      });
  }

  doSomethingOnClickTwo() {
    this.homePresenter.doSomethingOnlyInMobile();
  }

  triggerSomeHereInMobile() {

  }
}
