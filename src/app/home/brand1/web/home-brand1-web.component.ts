import { Component, OnInit, Inject } from '@angular/core';
import { HomePresenterBrand1WebConstract } from '../../home-contract.interface';
import { HomePresenter } from '../../home.presenter';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-home-brand1-web]',
  templateUrl: 'home-brand1-web.component.html'
})
export class HomeBrand1WebComponent implements OnInit {

  // @Inject because we want diffrent type for our presenter
  constructor(@Inject(HomePresenter) private homePresenter: HomePresenterBrand1WebConstract) {
    this.homePresenter.setContext(<any>this);
  }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.homePresenter.loadDataFromServer()
      .subscribe(data => {
        // do something here
      });
  }

  doSomethingOnClick() {
    this.homePresenter.doSomethingOnlyInWeb();
  }

  triggerSomeHereInWeb() {

  }

}
