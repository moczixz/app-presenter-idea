import { Observable } from 'rxjs';
import { HomeBrand1WebComponent } from './brand1/web/home-brand1-web.component';
import { HomeBrand1MobileComponent } from './brand1/mobile/home-brand1-mobile.component';

export type HomeComponentContext = HomeBrand1WebComponent & HomeBrand1MobileComponent;

// Children know only presenter properties belongs to them only
interface HomePresenterContract {
    setContext(context: HomeComponentContext);
    loadDataFromServer(): Observable<any>;
}

export interface HomePresenterBrand1WebConstract extends HomePresenterContract {
    doSomethingOnlyInWeb(): void;
}

export interface HomePresenterBrand1MobileContract extends HomePresenterContract {
    doSomethingOnlyInMobile(): void;
}
