import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { HomeBrand1WebComponent } from './brand1/web/home-brand1-web.component';
import { HomeBrand1MobileComponent } from './brand1/mobile/home-brand1-mobile.component';
import { HomePresenter } from './home.presenter';

@NgModule({
  declarations: [
    HomeComponent,
    HomeBrand1WebComponent,
    HomeBrand1MobileComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  providers: [
    HomePresenter
  ],
  entryComponents: [
    HomeBrand1WebComponent,
    HomeBrand1MobileComponent
  ]
})
export class HomeModule { }
