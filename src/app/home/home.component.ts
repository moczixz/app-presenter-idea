import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { ViewAnchorDirective } from '../shared/directives/view-anchor.directive';
import { HomeBrand1WebComponent } from './brand1/web/home-brand1-web.component';
import { ViewResolver } from '../shared/class/view-resolver';
import { HomeBrand1MobileComponent } from './brand1/mobile/home-brand1-mobile.component';

@Component({
  selector: 'app-home',
  template: '<ng-template view-anchor></ng-template>'
})
export class HomeComponent implements OnInit {

  @ViewChild(ViewAnchorDirective) viewAnchor: ViewAnchorDirective;

  private componentsMap = {
    'brand1-web': HomeBrand1WebComponent,
    'brand1-mobile': HomeBrand1MobileComponent
  };

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit() {
    const viewResolver = new ViewResolver(this.componentFactoryResolver, this.viewAnchor);
    viewResolver.resolveView(this.componentsMap);
  }

}
