import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HomeComponentContext } from './home-contract.interface';

// presenter know all his children
@Injectable()
export class HomePresenter {

  private context: HomeComponentContext;

  setContext(context: HomeComponentContext) {
    this.context = context;
  }

  loadDataFromServer(): Observable<any> {
    return of({});
  }

  doSomethingOnlyInWeb() {
    this.context.triggerSomeHereInWeb();
  }

  doSomethingOnlyInMobile() {
    this.context.triggerSomeHereInMobile();
  }

}
