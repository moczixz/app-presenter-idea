Angular7 Koncept Component-Presenter-Service + podzial danego komponentu na brand/apke(bonus)


Koncepcja prezenta jest dosyc prosta, dodany zostal jako kolejny serwis tylko z slowem Presenter a nie Service. W nim powinna byc cala logika ktora obecnie u nas siedzi w komponencie.
Prezenter wywolywanie jest przez komponent, i po ogarnieciu logiki wywoluje dana akcje komponentu. Komponent odpowiada tylko za UI, czyli w nim jest wstrzykiwany prezenter i serwisy odpowiedzialne za ui np snackbar-service.



Druga czesc to podzial apki na podkomponenty podzielone ze wzgledu na brand czy apke.
Wybor danego komponentu ogarniany jest podobnie jak u nas czyli wg hierarchi szablonow, i wstrzykiwany jest dynamicznie gdzie trzeba.
Zaleta jest taka ze mozemy uzywac w roznych brandach komponentow z innego brandu wg hierarchi, i sam komponent jest duzo czytelniejszy, bo np komponent webowy nie zawiera kodu odpowiedzialnego za mobile, np odpalanie mobilnych modili jako <select>



Wada tego rozwiazania na tym branchu (master) jest to ze wszystkie pod komponenty brand/app sa zbuildowane w jednym NgModule, wiec caly html jest ladowany do modulu ze wszystkich brandow i apek, mimo ze nie jest kompletnie uzywany. Rozwiazanie tego problemu jest w drugim branchu i tam dalszy opis: https://bitbucket.org/moczixz/app-presenter-idea/src/better-build/